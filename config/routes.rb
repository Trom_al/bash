Rails.application.routes.draw do

  resources :quotes, except: %i(destroy update edit) do

    collection do
      get :best
    end

    member do
      put :like
      put :dislike
    end

  end

end
