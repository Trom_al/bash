class Quote < ActiveRecord::Base

  BEST_RATING_BOUND = 1

  scope :best, -> { where('rating > ?', BEST_RATING_BOUND) }

  validates :description, presence: true

  def like!
    increment!(:rating)
  end

  def dislike!
    decrement!(:rating)
  end

end
