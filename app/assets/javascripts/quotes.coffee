# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$ ->
  $('.JS-Rating').click (e) ->
    e.preventDefault()
    elem = $(this)
    url = elem.data('url')
    $.ajax
      url: url
      type: 'PUT'
      success: (response) ->
        elem.siblings('.JS-Rating-value').first().html(response.rating)
