class QuotesController < ApplicationController

  before_action :set_quote, only: [:show, :like, :dislike]

  def index
    @quotes = Quote.order(created_at: :desc)
  end

  def best
    @quotes = Quote.best.order(rating: :desc)
    render :index
  end

  def show
  end

  def new
    @quote = Quote.new
  end

  def create
    @quote = Quote.new(quote_params)

    if @quote.save
      redirect_to @quote
    else
      render :new
    end
  end

  def like
    quote_service.like(@quote)
    render json: {
      success: true,
      rating: @quote.rating
    }
  end

  def dislike
    quote_service.dislike(@quote)
    render json: {
      success: true,
      rating: @quote.rating
    }
  end

  private

  def quote_service
    @quote_service ||= ::QuoteService.new(session)
  end

  def set_quote
    @quote = Quote.find(params[:id])
  end

  def quote_params
    params.require(:quote).permit(:description, :rating)
  end

end
