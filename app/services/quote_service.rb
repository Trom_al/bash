class QuoteService

  RATED_QUOTES_KEY = :rated_quotes

  def initialize(session)
    @session = session
  end

  def like(quote)
    update_rated_quotes(quote) { |record| record.like! }
  end

  def dislike(quote)
    update_rated_quotes(quote) { |record| record.dislike! }
  end

  private

  attr_reader :session

  def update_rated_quotes(quote)
    rated_quotes = session.fetch(RATED_QUOTES_KEY, [])

    return false if rated_quotes.include?(quote.id)

    yield quote

    rated_quotes << quote.id

    session[RATED_QUOTES_KEY] = rated_quotes
    true
  end

end
